extern crate ncurses;

use std::char;
use ncurses::*;

use srand::{Rand,RngSource};
use std::cmp::min;

const K_MAP_WIDTH: usize = 16;
const K_MAP_HEIGHT: usize = 16;

const K_MAX_ACTORS: usize = 16;
// therefore the player id is always at least 1 larger than any enemy id
const PLAYER_ID: usize = K_MAX_ACTORS;

const K_FLOOR: char = '.';
const K_WALL: char = '#';
const K_STAIRS: char = '<';


#[derive(Debug, PartialEq, Eq, Copy, Clone)]
struct Point {
    pub x: i32,
    pub y: i32,
}

impl std::ops::Sub<Point> for Point {
    type Output = Self;
    fn sub(self, other: Self) -> Self::Output {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}
impl std::ops::Add<Point> for Point {
    type Output = Self;
    fn add(self, other: Self) -> Self::Output {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
enum ActorType {
    Player,
    Enemy
}

type ActorId = usize;

#[derive(Copy, Clone)]
struct Actor {
    pub id: ActorId,
    position: Point,
    pub actor_type: ActorType,
}

impl Actor {
    fn glyph(&self) -> char {
        match self.actor_type {
            ActorType::Player => '@',
            ActorType::Enemy => 'e',
        }
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
struct Rect {
    anchor: Point,
    width: usize,
    height: usize,
}

impl Rect {
    fn shrink(&self, by: usize) -> Rect {
        Rect {
            anchor: Point {x: self.anchor.x + by as i32, y: self.anchor.y + by as i32},
            width: self.width - by,
            height: self.height - by,
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
enum Terrain {
    Floor,
    Wall,
    Stairs
}

impl Terrain {
    fn glyph(&self) -> char {
        match self {
            Terrain::Floor => '.',
            Terrain::Wall => '#',
            Terrain::Stairs => '<',
        }
    }
}

#[derive(Copy, Clone)]
struct Map {
    tiles: [[char; K_MAP_HEIGHT]; K_MAP_WIDTH],
    actor_by_tile: [[Option<ActorId>; K_MAP_HEIGHT]; K_MAP_WIDTH],
}

impl Map {
    fn tile(&self, cell: &Point) -> &char {
        &self.tiles[cell.y as usize][cell.x as usize]
    }

    fn tile_mut(&mut self, cell: &Point) -> &mut char {
        &mut self.tiles[cell.y as usize][cell.x as usize]
    }

    fn actor_by_tile(&self, cell: &Point) -> Option<ActorId> {
        self.actor_by_tile[cell.y as usize][cell.x as usize]
    }

    fn actor_by_tile_mut(&mut self, cell: &Point) -> &mut Option<ActorId> {
        &mut self.actor_by_tile[cell.y as usize][cell.x as usize]
    }

    fn is_unobstructed_tile(&self, tile: &Point) -> bool {
        *self.tile(tile) == K_FLOOR && self.actor_by_tile(tile).is_none()
    }

    fn try_move_actor(&mut self, actor: &mut Actor, to: &Point) -> bool {
        if self.is_unobstructed_tile(to) && &actor.position != to {
            *self.actor_by_tile_mut(&actor.position) = None;
            *self.actor_by_tile_mut(to) = Some(actor.id);
            // synchronize actors on map with actors array
            actor.position = *to;
            return true
        }
        false
    }

    fn random_spawn_pos(&mut self, rng: &mut Rand<RngSource>) -> Point {
        loop {
            let random_pos = Point {
                x: rng.int32() & (K_MAP_HEIGHT as i32 - 1),
                y: rng.int32() & (K_MAP_WIDTH as i32 - 2),
            };
            if self.is_unobstructed_tile(&random_pos) {
                return random_pos
            }
        }
    }

    fn fill(&mut self, glyph: char, rect: Rect) {
        for y in rect.anchor.y..(rect.height as i32) {
            for x in rect.anchor.x..(rect.width as i32) {
                *self.tile_mut(&Point {x, y}) = glyph;
            }
        }
    }

    // init the map as "empty" to imitate that mvaddstr draws lines terminated by null (!) in the
    // C version since the code does not draw the full map that the array would provide but leaves
    // elements null which terminate the draw operation
    fn new() -> Map {
        Map {
            tiles: [[' '; K_MAP_HEIGHT]; K_MAP_WIDTH],
            actor_by_tile: [[None; K_MAP_HEIGHT]; K_MAP_WIDTH]
        }
    }
}

fn range(low: i32, high: i32, rng: &mut Rand<RngSource>) -> i32 {
	(rng.int32() % (high - low)) + low
}

fn draw_map(map: &Map, player: &Actor, actors: &[Option<Actor>; K_MAX_ACTORS]) {
    clear();
    let mut map_with_actors = *map;
    // add the player and actors to the map representation
    *map_with_actors.tile_mut(&player.position) = player.glyph();
    for actor in actors.iter().filter(|a| !a.is_none()) {
        let actor = *actor;
        *map_with_actors.tile_mut(&actor.unwrap().position) = actor.unwrap().glyph();
    }
    // draw the map line by line (top to bottom)
    for (row, line) in map_with_actors.tiles.iter().enumerate() {
        mvaddstr(row as i32, 0, &String::from_iter(line));
    }
}

fn allocate_actor(actor: Actor, actors: &mut [Option<Actor>; K_MAX_ACTORS]) -> bool {
    for actor_space in actors.iter_mut() {
        if actor_space.is_none() {
            *actor_space = Some(actor);
            return true
        }
    }
    false
}

fn actor_by_id(id: ActorId, actors: &mut [Option<Actor>; K_MAX_ACTORS]) -> &mut Option<Actor> {
    &mut actors[id]
}

fn new_level(
    map: &mut Map,
    player: &mut Actor,
    actors: &mut [Option<Actor>; K_MAX_ACTORS],
    level: &mut usize,
    rng: &mut Rand<RngSource>
) {
    *map = Map::new();

    let dungeon = Rect{
        anchor: Point{ x: 0, y: 0},
        width: K_MAP_WIDTH - 2,
        height: K_MAP_HEIGHT - 1
    };
	map.fill(K_WALL, dungeon);
	map.fill(K_FLOOR, dungeon.shrink(1));

    // randomly select intersection point of the two walls that will separate the four rooms
    let intersection = Point {
        x: range(2, K_MAP_WIDTH as i32 - 4, rng),
        y: range(2, K_MAP_WIDTH as i32 - 4, rng),
    };

	// create horizontal walls and openings
	map.fill(K_WALL,
        Rect {
            anchor: Point { x: 1, y: intersection.y },
            width: dungeon.width,
            height: intersection.y as usize + 1
        });
    // create openings in horizontal walls
    *map.tile_mut(&Point{ x: range(1, intersection.x, rng)                         , y: intersection.y }) = K_FLOOR;
    *map.tile_mut(&Point{ x: range(intersection.x + 1, K_MAP_WIDTH as i32 - 3, rng), y: intersection.y }) = K_FLOOR;

	// create vertical walls and openings
	map.fill(K_WALL,
        Rect {
            anchor: Point { x: intersection.x, y: 1 },
            width: intersection.x as usize + 1,
            height: K_MAP_HEIGHT - 2
        });
    // create openings in vertical walls
    *map.tile_mut(&Point{ x: intersection.x, y: range(1, intersection.y, rng) }                          ) = K_FLOOR;
    *map.tile_mut(&Point{ x: intersection.x, y: range(intersection.y + 1, K_MAP_HEIGHT as i32 - 2, rng) }) = K_FLOOR;

    // create exit
    loop {
        let exit = Point {
            x: range(1, dungeon.width as i32, rng),
            y: range(1, dungeon.height as i32, rng),
        };
        if *map.tile(&exit) == K_FLOOR {
			*map.tile_mut(&exit) = K_STAIRS;
			break;
		}
    }
	*level += 1;

    // randomly place player and actors
    let random_pos: Point = map.random_spawn_pos(rng);
	player.position = random_pos;
    *map.actor_by_tile_mut(&player.position) = Some(player.id);
	for actor in actors.iter_mut() {
		*actor = None;
	}
	for id in 0..min(*level, 10) {
        let random_pos: Point = map.random_spawn_pos(rng);
        allocate_actor(Actor{ id , position: random_pos, actor_type: ActorType::Enemy }, actors);
        *map.actor_by_tile_mut(&random_pos) = Some(id);
	}
}

fn sign_of(a: i32) -> i32 {
	(a > 0) as i32 - (a < 0) as i32
}

fn done(msg: &str, level: &usize) {
	endwin();
	println!("{} {}.", msg, level);
	std::process::exit(0);
}

fn main() {
    let mut level: usize = 0;
    let mut input: i32;
    let mut map = Map::new();
    let mut player: Actor = Actor{ id: PLAYER_ID, position: Point { x: 0, y: 0 }, actor_type: ActorType::Player };
    let mut enemies: [Option<Actor>; K_MAX_ACTORS] = [None; K_MAX_ACTORS];

	initscr();
	cbreak();
	let mut rng: Rand<_> = Rand::new(RngSource::new(1));
	keypad(stdscr(), true);

    new_level(&mut map, &mut player, &mut enemies, &mut level, &mut rng);
    loop {
        draw_map(&map, &player, &enemies);

        // process the player
        {
            input = getch();

            let mut next_position = player.position;
            // since new_level() always inits (at least) the player position unwrap() can be
            // safely called here
            if input == KEY_DOWN  { next_position.y += 1; }
            if input == KEY_LEFT  { next_position.x -= 1; }
            if input == KEY_RIGHT { next_position.x += 1; }
            if input == KEY_UP    { next_position.y -= 1; }
            if input == i32::from(b'q') {
                done("Quit on level", &level);
            }

            match *map.tile(&next_position) {
                K_WALL => { continue; }
                K_STAIRS => {
                    new_level(&mut map, &mut player, &mut enemies, &mut level, &mut rng);
                    continue;
                }
                K_FLOOR => {
                    if let Some(id) = map.actor_by_tile(&next_position) {
                        if id != player.id {
                            // kill enemy
                            *map.tile_mut(&next_position) = K_FLOOR;
                            *map.actor_by_tile_mut(&next_position) = None;
                            *actor_by_id(id, &mut enemies) = None;
                            map.try_move_actor(&mut player, &next_position);
                        }
                    } else {
                        map.try_move_actor(&mut player, &next_position);
                    }
                }
                _ => { }
            }
        }

        // process enemies
        for actor in enemies.iter_mut().filter(|a| a.is_some()) {

            let mut next_position = actor.unwrap().position;
            // difference vector to player
            let delta: Point = player.position - next_position;
            // find direction of next nearest cell to reach player
            let nearest_cell = Point{ x: sign_of(delta.x), y: sign_of(delta.y)};

            // decide where the enemy will move next
            let directions_towards_player = [
                Point{ x: 0, y: nearest_cell.y },
                Point{ x: nearest_cell.x, y: 0 }
            ];
            for (i, dir) in directions_towards_player.iter().enumerate() {
                //clear();
                //mvaddstr(16, 0, &format!("E: {}, {}: ", actor.unwrap().position.x, actor.unwrap().position.y));
                //mvaddstr(16 + i as i32, 0, &format!("dir: {}, {}: ", dir.x, dir.y, ));
                //let _ = getch();
                // traverse if the next step is in the player direction and is not occupied by
                // another enemy
                let next_cell_in_direction: Point = next_position + *dir;
                if (nearest_cell.x != 0 || nearest_cell.y != 0)
                    && *map.tile(&next_cell_in_direction) == K_FLOOR {
                        match map.actor_by_tile(&next_cell_in_direction) {
                            Some(id) if id < PLAYER_ID => { continue; } // since only enemies have ids larger than 0
                            _ => {
                                next_position = next_cell_in_direction;
                                break;
                            }
                        }
                }
            }

            // end the game if the player is on the next chosen position
            if let Some(actor_id) = map.actor_by_tile(&next_position) {
                if actor_id == player.id {
                    done("Died on level", &level);
                }
            }
            if let Some(ref mut a) = actor {
                map.try_move_actor(a, &next_position);
            }
        }
    }
}
