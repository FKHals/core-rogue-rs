# Rust port of "core: a minimal roguelike" in Rust

This is a Rust port of the minimal roguelike "core" originally written in C (see [original source](https://www.roguelikeeducation.org/vault/core/core.c)).

## Description

Core generates levels consisting of up to four rooms and stairs leading to the next level. Movement is limited to cardinal directions (using the arrow keys), and there is no command to skip a turn. Bumping into the generic evil in the dungeon will remove it from the map; being bumped will remove you from the game. Pressing q will quit the game. [[source](https://www.roguelikeeducation.org/1.html)]

## Usage

Just start the game with:
```bash
make
```

## Requirements

* `cargo`
* `ncurses`

